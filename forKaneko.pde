int scWith = 600;
int squareNum = 5;
float gridWidth;
int N = 5;
Agent[] agents;
Agent[][] grid;

void setup() {
  agents = new Agent[N];
  gridWidth = scWith / squareNum;
  println(squareNum);
  grid = new Agent[squareNum][squareNum];
  for (int i = 0; i < squareNum; i ++) {
    for (int j = 0; j < squareNum; j++) {
      grid[i][j] = null;
    }
  }
  
  for (int i = 0; i < N; i++) {
    agents[i] = new Agent(i);
  }
  size(600, 600);
}

void draw() {
  background(255);
  for (Agent a : agents) {
    a.show();
  }
}

void mousePressed() {
 agents[0].move(Direction.UP); 
}