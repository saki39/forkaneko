  enum Direction {
    UP, DOWN, LEFT, RIGHT
  }

class Agent {
  int x, y;
  int ID;
  
  Agent(int _ID) {
    ID = _ID;
    while(grid[x][y] != null) {
      x = int(random(0, 1000)) % squareNum;
      y = int(random(0, 1000)) % squareNum;
      if (grid[x][y] != null) {
        print(grid[x][y].ID);
      }
    }
    grid[x][y] = this;
  }
  
  void show() {
    fill(200, 100, 100);
    ellipseMode(CENTER);
    ellipse(gridWidth * x + gridWidth/2, gridWidth * y + gridWidth/2, gridWidth, gridWidth);
  }
  
  void move(Direction d) {
    if (d == Direction.DOWN) {
       if(y + 1 < height && grid[x][y + 1] == null) {
         grid[x][y] = null;
         y ++;
       }
    }

    if (d == Direction.UP) {
      if(y - 1 >= 0 && grid[x][y - 1] == null) {
        grid[x][y] = null;
        y--;
      }
      else {
        move(Direction.DOWN);
      }
    }
  }
}
    